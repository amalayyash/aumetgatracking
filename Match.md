# distInitiateMatch

Initiation of distributor match from homepage, or link in menu

```json
{
  "event": "distInitiateMatch",
  "match": { "actionField": { "list": "src" }, "item": {} },
  "gtm.uniqueEventId": 8
}
```

Sources:

- Homepage
- Header
- Footer

# distStepMatch

### Step (select keyword)

```json
{
  "event": "distStepMatch",
  "match": {
    "actionField": { "step": 1 },
    "item": { "scientificName": "Rib Retractors" }
  },
  "gtm.uniqueEventId": 7
}
```

### Step (select medical line)

```json
{
  "event": "distStepMatch",
  "match": {
    "actionField": { "step": 1 },
    "item": { "line": "Disposables" }
  },
  "gtm.uniqueEventId": 8
}
```

### Step (select speciality)

```json
{
  "event": "distStepMatch",
  "match": {
    "actionField": { "step": 2 },
    "item": { "speciality": "Dermatology And Aesthetic Medicine" }
  },
  "gtm.uniqueEventId": 9
}
```

### Step (clients only)

```json
{
  "event": "distStepMatch",
  "match": {
    "actionField": { "step": 1 },
    "item": { "query": "clients only" }
  },
  "gtm.uniqueEventId": 9
}
```


### Step (Select country)

```json
{
  "event": "distStepMatch",
  "match": { "actionField": { "step": 3 }, "item": { "country": "France" } },
  "gtm.uniqueEventId": 10
}
```

### Step (select regions)

```json
{
  "event": "distStepMatch",
  "match": {
    "actionField": { "step": 4 },
    "item": { "regions": "Australia, Japan, South Korea & Singapore" }
  },
  "gtm.uniqueEventId": 11
}
```

### Step (select certificates)

```json
{
  "event": "distStepMatch",
  "match": {
    "actionField": { "step": 5 },
    "item": { "certificates": "FDA, CE Mark" }
  },
  "gtm.uniqueEventId": 12
}
```

# distFinalMatch

This is fired every time a result is shown

```json
{
  "event": "distFinalMatch",
  "match": {
    "actionField": { "list": "Distributor match" },
    "item": {
      "total": 4,
      "firstmatch": {
        "manufacturer": "Medical Leaders Establishment",
        "product": {
          "id": 15677,
          "name": "Baby Vitamin D Oral Solution ",
          "line": "Dietry Suplements"
        }
      }
    }
  },
  "gtm.uniqueEventId": 8
}
```
