# productInquire

Object sent (in product inquery, product and manufacturer are included)

```json
{
  "event": "productInquire",
  "inquire": {
    "actionField": { "list": "Src" },
    "item": {
      "product": {
        "id": 7234,
        "name": "Pediatric Stretcher Trolley – CARVI KIDS",
        "manufacturer": "BMB MEDICAL BMB MEDICAL",
        "scientificName": "Stretcher Trolleys",
        "line": "Disposable"
      },
      "manufacturer": {
        "id": 211678,
        "name": "BMB MEDICAL BMB MEDICAL",
        "country": {
          "id": 2,
          "name": "United Arab Emirates (UAE)",
          "countryCode": "AE"
        }
      },
      "distributor": null,
      "scientificName": null,
      "regions": null,
      "certificates": null,
      "position": 0
    }
  },
  "gtm.uniqueEventId": 19
}
```

The sources

- Homepage - products
- Medical line details - product list
- Scientific name details - product list
- Product details - top
- Product details - top sticky
- Product details - similar products
- Product details - manufacturer products
- Product search
- Manufacturer details

# manufacturerInquire

This also is created in distributor match page, where the items searched for are appended to the inquiry, they may or may not be present according to how deep the user searched.

```json
{
  "event": "manufacturerInquire",
  "inquire": {
    "actionField": { "list": "src" },
    "item": {
      "product": {
        "id": 15677,
        "name": "Baby Vitamin D Oral Solution ",
        "scientificName": "Vitamin D3",
        "line": "Dietry Suplements"
      },
      "manufacturer": {
        "id": "324234424",
        "name": "Medical Leaders Establishment",
        "country": { "id": 108, "name": "Italy", "countryCode": "IT" }
      },
      "distributor": null,
      "scientificName": null,
      "line": "Disposables",
      "speciality": "Dermatology And Aesthetic Medicine",
      "regions": ["Japan, South Korea & Singapore"],
      "certificates": ["CE Mark"],
      "position": 0
    }
  },
  "gtm.uniqueEventId": 6
}
```

Sources:

- Manufacturer details - top
- Manufacturer details - top sticky
- Manufacturer details - similar manufacturers
- Scientific name details - manufacturer list
- Manufacturer search
- Distributor match

# scientificNameInquire

This always comes with manufacturer info

```json
{
  "event": "scientificNameInquire",
  "inquire": {
    "actionField": { "list": "src" },
    "item": {
      "product": null,
      "manufacturer": {
        "id": 12345567,
        "name": "BMB MEDICAL BMB MEDICAL",
        "country": {
          "id": 2,
          "name": "United Arab Emirates (UAE)",
          "countryCode": "AE"
        }
      },
      "distributor": null,
      "scientificName": { "id": "2321313", "name": "vitamin C" },
      "regions": null,
      "certificates": null
    }
  },
  "gtm.uniqueEventId": 14
}
```

Sources:

- Manufacturer details

# distributorInquire

Not a real inquiry but rather user clicking to unlock a profile

```json
{
  "event": "distributorInquire",
  "inquire": {
    "actionField": { "list": "src" },
    "item": {
      "product": null,
      "manufacturer": null,
      "distributor": {
        "country": {
          "id": 2,
          "name": "United Arab Emirates (UAE)",
          "countryCode": "AE"
        },
        "specialities": [
          "Orthopedics Surgery",
          "Operating Room",
          "Ear Nose Throat, Ent ",
          "Gastroenterology",
          "Anesthesia & Resuscitation",
          "Immunomodulators And Antineoplastics"
        ]
      },
      "scientificName": null,
      "regions": null,
      "certificates": null,
      "position": 0
    }
  },
  "gtm.uniqueEventId": 41
}
```

Sources:

- Scientificname details - distributor list
- Product details - distributor list
- Distributor search
