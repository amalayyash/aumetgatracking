# productSearch

The object sent to this track is the following

```json
{
  "event": "productSearch",
  "search": {
    "actionField": { "list": "Product search" },
    "item": {
      "scientificNames": "Rib Retractors, Ribdsdsf Something, Some very long scientific name",
      "regions": "Australia, Japan, South Korea & Singapore",
      "lines": "Equipment, Healthcare IT",
      "specialities": "Anti Infective"
    }
  },
  "gtm.uniqueEventId": 13
}
```


# manufacturerSearch

```json
{
  "event": "manufacturerSearch",
  "search": {
    "actionField": { "list": "Manufacturer search" },
    "item": {
      "scientificNames": "Rib Retractors, Ribdsdsf Something, Some very long scientific name",
      "regions": "Australia, Japan, South Korea & Singapore",
      "lines": "Equipment, Healthcare IT",
      "specialities": "Anti Infective"
    }
  },
  "gtm.uniqueEventId": 7
}
```

# distributorSearch

```json
{
  "event": "distributorSearch",
  "search": {
    "actionField": { "list": "Distrubutor search" },
    "item": {
      "scientificNames": "Rib Retractors, Ribdsdsf Something, Some very long scientific name",
      "regions": null,
      "countries": "United Arab Emirates (UAE), Algeria, Egypt",
      "lines": null,
      "specialities": null
    }
  },
  "gtm.uniqueEventId": 7
}
```

