# ProductClick

The datalayer looks like this: (gaSrc could be one of the values listed below)

```json
{
  "event": "productClick",
  "click": {
    "actionField": { "list": "src" },
    "item": {
      "id": 7234,
      "name": "AV Tubing Sets Urology & Nephrology",
      "manufacturer": "BMB MEDICAL BMB MEDICAL",
      "scientificName": "Dialysis Sets",
      "line": "Disposable",
      "position": 1
    }
  },
  "gtm.uniqueEventId": 5
}
```

Here is a list of all "actionField.list" properties (gaSrc)

- Homepage - products
- Product search
- Product details - manufacturer products
- Product details - similar products
- Manufacturer details
- Scientificname details
- Line details - product list

# medicalLineClick

```json
{
  "event": "medicallineClick",
  "click": {
    "actionField": { "list": "src" },
    "item": { "name": "Disposable", "position": 0 }
  },
  "gtm.uniqueEventId": 6
}
```

srouces:

- Medical line details - dropdown
- Medical line details - product list
- Product details - top
- Product details - similar products
- Product details - manufacturer products
- Manufacturer details
- Product search
- Sientific name details - top
- Homepage - products
- Homepage - lines
- Homepage - lines mobile
- Header
- Footer - lines

# scientificnameClick

```json
{
  "event": "scientificNameClick",
  "click": {
    "actionField": { "list": "src" },
    "item": { "id": 77510, "name": "Stretcher Trolleys", "position": 0 }
  },
  "gtm.uniqueEventId": 68
}
```

srouces:

- Medical line details - product list
- Scientific name details - product list
- Scientific name details - related
- Scientific name details - similar scinames
- Product details - top
- Product details - similar products
- Product details - manufacturer products
- Homepage - products

# manufacturerClick

```json
{
  "event": "manufacturerClick",
  "click": {
    "actionField": { "list": "src" },
    "item": {
      "id": 211678,
      "name": "BMB MEDICAL BMB MEDICAL",
      "country": {
        "id": 2,
        "name": "United Arab Emirates (UAE)",
        "countryCode": "AE"
      },
      "position": 0
    }
  },
  "gtm.uniqueEventId": 11
}
```

Sourcs:

- Homepage - products
- Manufacturer details - similar manufacturers
- Product details - top
- Product details - top sticky
- Product details - similar products
- Product details - manufacturers products tail
- Manufacturer search
- Product search
- Scientific name details - product list
- Scientific name details - manufacturer list
- Medical line details - product list
  > Following receive only "name" of partner inside item
- Homepage - partners
- Distributores match - partners

# specialityClick

```json
{
  "event": "specialityClick",
  "click": {
    "actionField": { "list": "src" },
    "item": { "name": "Gynecology & Obstetrics", "position": 8 }
  },
  "gtm.uniqueEventId": 6
}
```

Sources:

- Medical line details - speciality list
- Homepage - lines

# documentClick

```json
{
  "event": "documentClick",
  "click": {
    "actionField": { "list": "src" },
    "item": { "name": "Product catalog", "link": "link", "position": 8 }
  },
  "gtm.uniqueEventId": 6
}
```

Soruces

- Manufacturer details
- Product details - gallery
- Product details - documents

# productSearch

Those are clicks to initiate a search

```json
{
  "event": "productSearch",
  "click": { "actionField": { "list": "src" }, "item": {} },
  "gtm.uniqueEventId": 14
}
```

Sources:

- Homepage - products
- Medical line details - products list
- Product details - similar products
- Scientificname details - products stats
- Scientificname details - product list stail



# distributorSearch

Those are clicks to initiate a search

```json
{
  "event": "distributorSearch",
  "click": { "actionField": { "list": "src" }, "item": {} },
  "gtm.uniqueEventId": 14
}
```

Sources:

- Scientificname details - distribuitor stats
- Scientificname details - distributor list stail


# manufacturerSearch

Those are clicks to initiate a search

```json
{
  "event": "manufacturerSearch",
  "click": { "actionField": { "list": "src" }, "item": {} },
  "gtm.uniqueEventId": 14
}
```

Sources:

- Scientificname details - manufacturer stats
- Scientificname details - manufacturer list stail


# legacyClick

Those are clicks to the legacy url (business.aumet)

```json
{
  "event": "legacyClick",
  "click": { "actionField": { "list": "src" } },
  "gtm.uniqueEventId": 14
}
```

Sources

- Homepage - manufacturer match
- Footer - manufacturer match
- Header - manufacturer match
- Footer - About aumet
- Header - About aumet
- Header - sign in
- Header - side menu sign in


# navigationClick

Clicks in navigation
```json
{
  "event": "navigationClick",
  "click": { "actionField": { "list": "src" } },
  "gtm.uniqueEventId": 14
}
```

Sources:

- Footer - Blog
- Header - Blog
- Footer - Privacy
- Header - logo
- Header - side menu logo
- Header - side menu search


# generalClick

```json
{
  "event": "generalClick",
  "click": { "actionField": { "list": "src" } },
  "gtm.uniqueEventId": 14
}
```

Sources

- Footer - facebook, or youtube, or twitter, or linkedin
- Homepage - partners scroll
- Manufacturer details - images scroll
- Product details - images scroll
- Manufacturer details - website click (this also comes with `item: {url: 'website url'}`)
- Manufacturer details - show more
- Distributor match - more details
- Manufacturer view dialog - view profile
- Manufacturer search - filter
- Product search - filter
- Distributor search - filter
- Product search - page (this also comes with `item: {page: 2}`)
- Manufacturer search - page (this also comes with `item: {page: 2}`)
- Distributor search - page (this also comes with `item: {page: 2}`)



