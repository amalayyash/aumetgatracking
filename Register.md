# initiateRegister

On load of registration page, the data layer looks like this

If registration comes from an inquiry button the item appearS according to the inquery made (same as those listed in [Inquire event page](Inquire.md))

```json
{
  "event": "initiateRegister",
  "register": {
    "actionField": { "list": "src" },
    "item": {}
  },
  "gtm.uniqueEventId": 34
}
```

Here is a list of all "actionField.list" properties (gaSrc)

- All inquery sourcs are repeated here
- Header
- Header - side menu
- Homepage
- Product details - unlock distributors
- Scientificname details - Register as distributor
- Scientificname details - Register as manufacturer
- Product search - no results
- Manufacturer search - no results
- Distributor search - no results
- Medical line details - no results

# stepRegister

### On submit of first step

```json
{
  "event": "stepRegister",
  "register": {
    "actionField": { "step": 1 },
    "item": {
      "name": "Amal Ayyash",
      "country": {
        "id": 2,
        "name": "United Arab Emirates (UAE)",
        "countryCode": "AE"
      },
      "phone": "5656656",
      "userType": "distributor",
      "email": "ayyash+newemail@gmail.com",
      "company": null
    }
  },
  "gtm.uniqueEventId": 26
}
```

### second step

```json
{
  "event": "stepRegister",
  "register": {
    "actionField": { "step": 2 },
    "item": {
      "name": "Amal Ayyash",
      "country": {
        "id": 2,
        "name": "United Arab Emirates (UAE)",
        "countryCode": "AE"
      },
      "phone": "56666",
      "userType": "distributor",
      "email": "ayyash+newemail@gmail.com",
      "jobTitle": "Business Developer",
      "company": {
        "id": "324234424",
        "name": "Medical Leaders Establishment",
        "country": { "id": 108, "name": "Italy", "countryCode": "IT" }
      }
    }
  },
  "gtm.uniqueEventId": 8
}
```

### Third step

```json
{
  "event": "stepRegister",
  "register": {
    "actionField": { "step": 3 },
    "item": {
      "name": "Amal Ayyash",
      "country": {
        "id": 2,
        "name": "United Arab Emirates (UAE)",
        "countryCode": "AE"
      },
      "phone": "56666",
      "userType": "distributor",
      "email": "ayyash+newemail@gmail.com",
      "jobTitle": "Business Developer",
      "company": {
        "id": "324234424",
        "name": "Medical Leaders Establishment",
        "country": { "id": 108, "name": "Italy", "countryCode": "IT" },
        "specialities" ["Anesthesia & Resuscitation", "Cardiac"]
      }
    }
  },
  "gtm.uniqueEventId": 10
}
```

# completeRegister

Upon laoding of "Thank you page" and finalized registration

```json
{
  "event": "completeRegister",
  "register": { "actionField": {}, "item": { "token": "sdfsdfsfdsfdsfsf" } },
  "gtm.uniqueEventId": 12
}
```

# stepRegister

When errors occur, just add to the field name to errors array (optional)

```json
{
  "event": "stepRegister",
  "register": {
    "actionField": { "step": 1 },
    "item": { "errors": ["fullName", "phone"] }
  },
  "gtm.uniqueEventId": 23
}
```
