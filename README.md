This is where I will be referecing all tags, their names and contents. In progress.

# Tracks

- [Click events](Click.md)
- [Inquire events](Inquire.md)
- [Register events](Register.md)
- [Lookup events](Lookup.md)
- [Search events](Search.md)
- [Match event](Match.md)