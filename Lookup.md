# scientificNameLookup

When scientific name is keyed in at least 2 charactes, the following is tracked

```json
{
  "event": "scientificNameLookup",
  "lookup": { "actionField": { "list": "src" }, "item": { "term": "ddgg" } },
  "gtm.uniqueEventId": 12
}
```

Sources:

- Header
- Manufacturer search
- Distrubutor search
- Product search
- Distributor match
- Homepag

# countryLookup

```json
{
  "event": "countryLookup",
  "lookup": { "actionField": { "list": "src" }, "item": { "term": "ddgg" } },
  "gtm.uniqueEventId": 12
}
```

Sources:

- Register - step 1
- Register - step 2
- Distributor match
- Product search - filter
- Manufacturer search - filter

# titleLookup

```json
{
  "event": "titleLookup",
  "lookup": { "actionField": { "list": "src" }, "item": { "term": "ddgg" } },
  "gtm.uniqueEventId": 12
}
```

Sources

- Register - step 2

# companyLookup

```json
{
  "event": "companyLookup",
  "lookup": { "actionField": { "list": "src" }, "item": { "term": "ddgg" } },
  "gtm.uniqueEventId": 12
}
```

Sources

- Register - step 2
